"""
2019-07-21
최종 목적 : 부산시 cctv 데이터와 부산시 인구수 데이터를 토대로 비교 분석
현재 코드 목적 : 부산시 cctv 데이터 셋에 구별 cctv 갯수만 정제
전체 책임자 및 개발자 : 이동현
유의점 : cctv를 지역당 하나로 체크했다. 그 지역을 전체 볼 수 만 있으면 상관 없을 것이라고 생각..
(한곳에 여러개 설치된 곳도 있다.)
"""
import pandas as pd

cctv = pd.read_csv('busan_cctv_re.csv', header= 0, encoding='cp949')

print(cctv.head())

grouped_cctv = cctv.groupby('구군')

out_cctv = pd.DataFrame(columns=('구군','cctv갯수'))

out_cctv['구군'] = grouped_cctv.count().index
out_cctv['cctv갯수'] = grouped_cctv.count().values

print(out_cctv)

out_cctv.to_csv('busan_cctv_count.csv',index=None,encoding='cp949')