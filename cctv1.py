"""
2019-07-21
최종 목적 : 부산시 cctv 데이터와 부산시 인구수 데이터를 토대로 비교 분석
현재 코드 목적 : 부산시 cctv 데이터 셋 정제
전체 책임자 및 개발자 : 이동현
"""

import pandas as pd

#부산시 cctv
cctv = pd.read_csv('busan_cctv.csv',header=0,encoding='utf-8')

#구번 주소 지우기
del cctv['소재지지번주소']

#도로명 주소의 구만 추출하기
i = 0
for index in cctv['소재지도로명주소']:
    i_str = index[6:10] #왜 split이 안되는지 모르겠다. ㅠㅠ
    i_str.strip()
    if i_str[2] == ' ':
        i_str = i_str[0:2]
    elif i_str[3] == ' ':
        i_str = i_str[0:3]
    cctv['소재지도로명주소'][i] = i_str
    i = i + 1

#columns 이름 바꾸기
cctv.rename(columns={cctv.columns[1] : '구군'}, inplace = True)

print(cctv.head())

#관리기관명 부산시 빼자..
i = 0
for index in cctv['관리기관명']:
    i_str = index[6:]
    i_str.strip()

    cctv['관리기관명'][i] = i_str
    i = i + 1

print(cctv.head())
print(cctv.tail())

cctv.to_csv('busan_cctv_re.csv',index = False, encoding='cp949')