"""
2019-07-21
최종 목적 : 부산시 cctv 데이터와 부산시 인구수 데이터를 토대로 비교 분석
현재 코드 목적 : 부산시 인구수 데이터 정리
전체 책임자 및 개발자 : 이동현
참고로 인구수 데이터는 2019년 3월 데이터이다.
"""

import pandas as pd

people = pd.read_csv('busan_people.csv',header=0,encoding='cp949')

#헤더파일의 위험물질을 제거해보자
people.rename(columns = {people.columns[1] : '동수'}, inplace=True)
people.rename(columns = {people.columns[2] : '세대수'}, inplace=True)
people.rename(columns = {people.columns[3] : '인구수'}, inplace=True)
people.rename(columns = {people.columns[4] : '남자인구수'}, inplace=True)
people.rename(columns = {people.columns[5] : '여자인구수'}, inplace=True)
people.rename(columns = {people.columns[6] : '시전체_인구에_대한_구성비'}, inplace=True)
people.rename(columns = {people.columns[7] : '면적'}, inplace=True)
people.rename(columns = {people.columns[8] : '인구밀도'}, inplace=True)

people.to_csv('busan_people_re.csv', encoding = 'cp949', index= False)