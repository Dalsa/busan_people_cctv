"""
2019-07-21
최종 목적 : 부산시 cctv 데이터와 부산시 인구수 데이터를 토대로 비교 분석
현재 코드 목적 : 인구수와 cctv 데이터 합쳐서 분석하기
전체 책임자 및 개발자 : 이동현
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import Krplt

#정제한 데이터 불러오기
cctv = pd.read_csv('busan_cctv_count.csv', header= 0, encoding='cp949')
people = pd.read_csv('busan_people_re.csv', header=0, encoding='cp949')

#데이터 합치기
data_result = pd.merge(cctv, people, on='구군')

#필요없은 요소 제거

#밑은 만약 busan_cctv_re.csv 파일로 조사시 사용하길 바랍니다.
# del data_result['관리기관명']
# del data_result['카메라화소수']
# del data_result['촬영방면정보']
# del data_result['보관일수']
# del data_result['설치년월']
# del data_result['위도']
# del data_result['경도']
# del data_result['데이터기준일자']
# del data_result['관리기관전화번호']
# del data_result['카메라대수']

del data_result['동수']
del data_result['시전체_인구에_대한_구성비']
del data_result['면적']

#구분을 쉽게 하기 위해서 구군을 index행
data_result.set_index('구군',inplace=True)

#상관 계수를 분석
cc_all_people = np.corrcoef(data_result['인구수'],data_result['cctv갯수'])
cc_man_people = np.corrcoef(data_result['남자인구수'],data_result['cctv갯수'])
cc_woman_people = np.corrcoef(data_result['여자인구수'],data_result['cctv갯수'])
cc_household = np.corrcoef(data_result['세대수'],data_result['cctv갯수'])

print("===전체 인구수의 상관계수===")
print(cc_all_people)
print("===전체 남자 인구수의 상관계수===")
print(cc_man_people)
print("===전체 여자 인구수의 상관계수===")
print(cc_woman_people)
print("===전체 세대수의 상관계수===")
print(cc_household)


#인구 밀도는 정제를 해서 봐야하기 때문에. 나두고 (필요시 정제 활용)
#결과 : 전체다 0.6 이상의 다소 높은 양적 선형관계를 유지한다.

#각각 요소별로 많은 수의 구를 비교해보자
print("===cctv 갯수 높은 순위 별===")
print(data_result.sort_values(by='cctv갯수', ascending=False).head(5))
print("===인구수 높은 순위 별===")
print(data_result.sort_values(by='인구수', ascending=False).head(5))
print("===남자인구수 높은 순위 별===")
print(data_result.sort_values(by='남자인구수', ascending=False).head(5))
print("===여자인구수 높은 순위 별===")
print(data_result.sort_values(by='여자인구수', ascending=False).head(5))
print("===세대수 높은 순위 별===")
print(data_result.sort_values(by='세대수', ascending=False).head(5))

#결과 : 해운대구, 부산진구, 남구 가 cctv가 많은 편이며 또한 인구수 많은 편에 속한다. (다소 애매함)

#mataplot 한글화
Krplt.KrPlt()

#그래프 그림 그리기
# data_result['cctv갯수'].plot(kind='barh', grid=True, figsize=(7,7))

data_result['cctv갯수'].sort_values().plot(kind='barh',grid = True, figsize=(10,10)) #정렬함
plt.show()

#scatter(점 찍기를 이용해서 찍어보기.. s는 사이즈 인듯하다.)
plt.figure(figsize=(6,6))
plt.scatter(data_result['인구수'],data_result['cctv갯수'], s = 50)
plt.xlabel('인구수')
plt.ylabel('cctv 갯수')
plt.grid()
plt.show()

#polyfit를 이용하엿 선형 회귀를 한다.
fp1 = np.polyfit(data_result['인구수'], data_result['cctv갯수'], 1)

#선형을 더 그리기 위해서 x축 데이터와 y축 데이터가 필요하다.
f1 = np.poly1d(fp1) #y 축 데이터 만들기 위함
fx = np.linspace(100000, 700000, 100) #x축 데이터 만들기 위함

plt.figure(figsize=(10,10))
plt.scatter(data_result['인구수'],data_result['cctv갯수'], s=50)
plt.plot(fx, f1(fx), ls='dashed', lw=3, color='g')
plt.xlabel('인구수');
plt.ylabel('cctv 갯수')
plt.grid()
plt.show()

#똑같이 중심축을 그림
fp1 = np.polyfit(data_result['인구수'], data_result['cctv갯수'],1)

#x와 y축 데이터를 구해온다.
f1 = np.poly1d(fp1)
fx = np.linspace(100000, 700000, 100)

#오차라는 열을 만들어서 넣어준다.
data_result['오차'] = np.abs(data_result['cctv갯수'] - f1(data_result['인구수']))

df_sort = data_result.sort_values(by='오차', ascending=False)
print(df_sort)

plt.figure(figsize=(14,10))
plt.scatter(data_result['인구수'],data_result['cctv갯수'], c = data_result['오차'], s = 50)
plt.plot(fx,f1(fx),ls='dashed', lw=3, color='g')

for n in range(10):
    plt.text(df_sort['인구수'][n]*1.02, df_sort['cctv갯수'][n]*0.98, df_sort.index[n], fontsize=15)

plt.xlabel('인구수')
plt.ylabel('인구당비율')

plt.colorbar()
plt.grid()
plt.show()
